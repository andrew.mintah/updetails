package com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.adapter;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ecobank.digx.cz.extxface.UPDetails.adapter.IUPDetailsAdapter;
import com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.assembler.UPDetailsAssembler;
import com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.responsehandler.UPDetailsAdapterResponseHandler;

import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsExtResponseDTO;
import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsRequestDTO;
import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsResponseDTO;
import com.ofss.digx.app.adapter.AbstractAdapter;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.datatype.complex.Party;
import com.ofss.digx.extxface.impl.AbstractAdapterHelper;
import com.ofss.extsystem.business.extsystems.HostAdapterHelper;
import com.ofss.extsystem.business.extsystems.HostAdapterManager;
//import com.ofss.extsystem.business.extsystems.rtd.dto.RequestDTO;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.dto.UserContextDTO;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fcubs124.service.fcubscustomerservice.MODIFYCUSTOMERFSFSREQ;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.UserContextDTO;
import com.ofss.extsystem.dto.RequestDTO;
import com.ofss.extsystem.dto.UserContextDTO;

public class UPDetailsAdapter extends AbstractAdapter implements IUPDetailsAdapter {
	
	private static final String THIS_COMPONENT_NAME = UPDetailsAdapter.class.getName();
	private final transient MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	private transient Logger logger  = this.formatter.getLogger(THIS_COMPONENT_NAME);
	private UPDetailsAdapterResponseHandler responseHandler = new UPDetailsAdapterResponseHandler();



	@Override
	public UPDetailsResponseDTO readParty(String var1) throws Exception {	
	
		UPDetailsResponseDTO responseDTO = null;
		super.checkRequest("com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.adapter",
				new Object[]{var1});
		AdapterInteraction.begin();
	    HostResponseDTO hostResponse = null;
	    HostRequestDTO hostRequest = null;
	    try {
	      System.out.println("fetch customer detail UDF - " + var1);
	      UPDetailsAssembler assembler = new UPDetailsAssembler();
	      hostRequest = assembler.fromAdaptertoHostRequestFetchCustomerDetail(var1, "ENG");
	      hostResponse = HostAdapterManager.processRequest(hostRequest);
	      UPDetailsExtResponseDTO custResponse = (UPDetailsExtResponseDTO)hostResponse.response;
	      System.out.println("fetch customer detail UDF Result " + custResponse.getUdf2());
	      responseDTO = new UPDetailsResponseDTO();
	      responseDTO.setUdf2(custResponse.getUdf2());
	    } catch (Exception e) {
	      if (logger.isLoggable(Level.SEVERE))
	        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch UDF", new Object[0]), e); 
	    } finally {
	      AdapterInteraction.close();
	    } 
	    checkResponse(responseDTO);
	    return responseDTO;
		
		
	

	}
	
	public UPDetailsResponseDTO postUdf2 (String var1, String Udf2) throws Exception {
	
		UPDetailsResponseDTO responseDTO = null;
		super.checkRequest("com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.adapter",
				new Object[]{var1});
		AdapterInteraction.begin();
	    HostResponseDTO hostResponse = null;
	    MODIFYCUSTOMERFSFSREQ hostRequest = null;
	    try {
	      System.out.println("post customer detail UDF - " + var1);
	      UPDetailsAssembler assembler = new UPDetailsAssembler();
	      hostRequest = assembler.fromModifyPartyDetailsDTOTOUBSObjectRequest(var1,Udf2);
	     // hostResponse = HostAdapterManager.processRequest(hostRequest);
	      /*UPDetailsExtResponseDTO custResponse = (UPDetailsExtResponseDTO)hostResponse.response;
	      System.out.println("post customer detail UDF Result " + custResponse.getUdf2());
	      responseDTO = new UPDetailsResponseDTO();
	      responseDTO.setUdf2(custResponse.getUdf2());*/
	    } catch (Exception e) {
	      if (logger.isLoggable(Level.SEVERE))
	        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed post UDF", new Object[0]), e); 
	    } finally {
	      AdapterInteraction.close();
	    } 
	    checkResponse(responseDTO);
	    return responseDTO;
			}


}
