package com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.assembler;

import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsRequestDTO;
import com.ofss.digx.datatype.complex.Party;
import com.ofss.digx.enumeration.ModuleType;
import com.ofss.fc.framework.domain.IAbstractDomainObject;
import com.ofss.fc.framework.domain.assembler.AbstractAssembler;
import com.ofss.fc.framework.domain.common.dto.DomainObjectDTO;
import com.ofss.fc.infra.exception.FatalException;
import com.ofss.digx.extxface.ubs124.impl.AbstractAdapterHelper;
import com.ofss.digx.extxface.ubs124.impl.RequestHeader;
import com.ofss.extsystem.business.extsystems.HostAdapterHelper;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.RequestDTO;
import com.ofss.extsystem.dto.UserContextDTO;
import com.ofss.fcubs124.service.fcubscustomerservice.CustomerFullType;
import com.ofss.fcubs124.service.fcubscustomerservice.FCUBSHEADERType;
import com.ofss.fcubs124.service.fcubscustomerservice.MsgStatType;
import com.ofss.fcubs124.service.fcubscustomerservice.UBSCOMPType;
import com.ofss.fcubs124.service.fcubscustomerservice.MODIFYCUSTOMERFSFSREQ.FCUBSBODY;

import com.ofss.fcubs124.service.fcubscustomerservice.*;


public class UPDetailsAssembler extends AbstractAssembler{
	
	
	
	public MODIFYCUSTOMERFSFSREQ fromModifyPartyDetailsDTOTOUBSObjectRequest(String customerId, String Udf2) throws com.ofss.digx.infra.exceptions.Exception {
		MODIFYCUSTOMERFSFSREQ modifyCUSTOMERFSFSREQ = new MODIFYCUSTOMERFSFSREQ();
	      AbstractAdapterHelper helper = AbstractAdapterHelper.getInstance();
	      FCUBSBODY fcUBSBody = new FCUBSBODY();
	      RequestHeader requestHeader = helper.getRequestHeader(ModuleType.SMS.getValue(), "FCUBSCustomerService", "ModifyCustomer");
	      FCUBSHEADERType fcUBSHEADER = new FCUBSHEADERType();
	      fcUBSHEADER.setBRANCH("ENG");
	      fcUBSHEADER.setSERVICE(requestHeader.getService());
	      fcUBSHEADER.setOPERATION(requestHeader.getOperation());
	      fcUBSHEADER.setDESTINATION(requestHeader.getDestination());
	      fcUBSHEADER.setSOURCE(requestHeader.getSource());
	      fcUBSHEADER.setUBSCOMP(UBSCOMPType.fromValue(requestHeader.getUbscomp()));
	      fcUBSHEADER.setMSGSTAT(MsgStatType.fromValue(requestHeader.getMsgstat()));
	      fcUBSHEADER.setUSERID(requestHeader.getUserid());
	      CustomerFullType customerFullType = new CustomerFullType();
	      customerFullType.setCUSTNO(customerId);
	      customerFullType.setUDF2(Udf2);
	    
	      fcUBSBody.setCustomerFull(customerFullType);
	      modifyCUSTOMERFSFSREQ.setFCUBSBODY(fcUBSBody);
	      modifyCUSTOMERFSFSREQ.setFCUBSHEADER(fcUBSHEADER);
	      return modifyCUSTOMERFSFSREQ;
	   }
	
	
	  public HostRequestDTO fromAdaptertoHostRequestFetchCustomerDetail(String customerId, String affCode) throws Exception {
		    UPDetailsRequestDTO hostRequestDTO = new UPDetailsRequestDTO();
		    hostRequestDTO.userContext = new UserContextDTO();
		    hostRequestDTO.userContext.idEntity = "B001";
		    hostRequestDTO.userContext.refIdEntity = "B001";
		    hostRequestDTO.userContext.idRequest = "DIGX_CZ_CUST_DETAIL_UDF";
		    hostRequestDTO.party = customerId;
		    return HostAdapterHelper.buildHostRequest((RequestDTO)hostRequestDTO);
		  }


	@Override
	public DomainObjectDTO fromDomainObject(IAbstractDomainObject arg0) throws FatalException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public IAbstractDomainObject toDomainObject(DomainObjectDTO arg0) throws FatalException {
		// TODO Auto-generated method stub
		return null;
	}
}
