package com.ecobank.digx.cz.business.extsystems.adapter;

import java.sql.Connection;
import java.util.ArrayList;

import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsExtResponseDTO;
import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsRequestDTO;
import com.ofss.extsystem.business.extsystems.HostAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterConstants;
import com.ofss.extsystem.business.extsystems.HostAdapterHelper;
import com.ofss.extsystem.business.extsystems.rtd.dto.ResponseDTO;
import com.ofss.extsystem.business.utils.ServiceUtils;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCEngine;
import com.ofss.extsystem.framework.utils.dbaccess.JDBCResultSet;

public class UPDetailsExtAdapter implements HostAdapter, HostAdapterConstants {
	private static final String SEL_UDF2_FOR_CUSTOMER = 
			"select udf2 from fcat_vw_cust_udf2_details where customer_no = ?";

	public HostResponseDTO processRequest(HostRequestDTO p_request) throws Exception {
	System.out.println("Manu Debug Area");
		/*ArrayList<String> l_args = null;
		UPDetailsRequestDTO l_updet_request = null;
		UPDetailsExtResponseDTO l_updet_response = null;
		HostResponseDTO l_host_response = null;
		JDBCResultSet l_rs = null;
		Connection l_con = null;

		try {
			l_args = new ArrayList();
			l_updet_request = (UPDetailsRequestDTO) p_request.hostRequest;
			l_con = p_request.txnContext.getConnection("A1");
			l_host_response = HostAdapterHelper.copyRequestToResponse(p_request);
			l_args.add(l_updet_request.party);
			l_rs = 	JDBCEngine.executeQuery(SEL_UDF2_FOR_CUSTOMER,
					l_args.size(), l_args, l_con);
			l_updet_response = new UPDetailsExtResponseDTO();
			if (!l_rs.next()) {
				ServiceUtils.addErrorToResponse(l_updet_request, l_updet_response, ServiceUtils.getError("10001",
						l_updet_request.userContext.idLang, l_updet_request.userContext.idDevice));
				l_updet_response.result.returnCode = 1;
				l_host_response.hostResponse = l_updet_response;
				HostResponseDTO var8 = l_host_response;
				return var8;
			}

			l_updet_response.setUdf(l_rs.getString("udf2"));
			System.out.println("Raks Debug Here" + l_updet_response);
			
		} finally {
			l_con.close();
		}

		l_updet_response.result.returnCode = 0;
		l_host_response.hostResponse = l_updet_response;
		return l_host_response;
	}*/
		
		 HostResponseDTO l_hresp = new HostResponseDTO();
		 UPDetailsRequestDTO request = null;
		 UPDetailsExtResponseDTO response = null;
		    request = (UPDetailsRequestDTO)p_request.hostRequest;
		    request.userContext = p_request.request.userContext;
		    response = new UPDetailsExtResponseDTO();
		    Connection l_con = null;
		    l_con = p_request.txnContext.getConnection("A1", p_request.idEntity, true);
		    JDBCResultSet l_rs = null;
		    ArrayList<Object> l_args = new ArrayList();
		    StringBuffer l_query = null;
		    l_query = new StringBuffer("select udf2 from fcat_vw_cust_udf2_details where customer_no = ? ");
		    l_args.add(request.party);
		    l_rs = JDBCEngine.executeQuery(l_query.toString(), l_args.size(), l_args, l_con);
		    if (l_rs.next())
		      if (l_rs.getString("udf2") != null) {
		        response.setUdf2(l_rs.getString("udf2"));
		      } else {
		        response.setUdf2(""); 
		      }  
		    l_hresp.hostResponse = (com.ofss.extsystem.dto.ResponseDTO)response;
		    response.result.returnCode = 0;
		    return l_hresp;
		  }

}
