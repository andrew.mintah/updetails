package com.ecobank.digx.cz.appx.UPDetails.service;

import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.util.logging.Level;

import javax.ws.rs.*;

import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsResponseDTO;
import com.ecobank.digx.cz.app.UPDetails.service.*;
import com.ofss.digx.app.context.ChannelContext;
import com.ofss.digx.app.core.ChannelInteraction;
import com.ofss.digx.appx.AbstractRESTApplication;
import com.ofss.digx.appx.PATCH;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.digx.datatype.complex.Party;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

import java.util.logging.Level;
import java.util.logging.Logger;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;


@Path("/updetails")
@Tag(name = "UDF Details", description = "REST API's Containing UDF related operations.")


public class UPDetails extends AbstractRESTApplication implements IUPDetails{
	
	private static final String THIS_COMPONENT_NAME = UPDetails.class.getName();
	private MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
    private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	
	
	@GET
	@Produces({"application/json"})
	@Path("/{partyId}")
	@Operation(summary = "Retrieves UDF2 of the customer", description = "Retrieves UDF2 of the customer", operationId = "com.ecobank.digx.cz.appx.UPDetails.service")

	public Response fetchUPDetails(@PathParam("partyId") Party party) throws java.lang.Exception 
		 {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, this.formatter.formatMessage(
					"Entered into fetchTransactions in REST %s: partyId=%s",
					new Object[]{THIS_COMPONENT_NAME, party }));
		}

		ChannelContext channelContext = null;
		Response response = null;
		UPDetailsResponseDTO upDetailsResponse = new UPDetailsResponseDTO();
		ChannelInteraction channelInteraction = ChannelInteraction.getInstance();

		try {
			channelContext = super.getChannelContext();
			channelInteraction.begin(channelContext);
		
			com.ecobank.digx.cz.app.UPDetails.service.UPDetails upDetailsService = new com.ecobank.digx.cz.app.UPDetails.service.UPDetails();
			upDetailsResponse  = upDetailsService.fetchUPDetails(channelContext.getSessionContext(),party);
			response = this.buildResponse(upDetailsResponse, Response.Status.OK);
		} catch (Exception var25) {
			logger.log(Level.SEVERE,
					this.formatter.formatMessage(
							"Exception encountered while invoking the service %s for upDetailsRequestDTO=%s",
							new Object[]{UPDetails.class.getName(), party}),
					var25);
			response = this.buildResponse(var25, Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception var24) {
				logger.log(Level.SEVERE, this.formatter.formatMessage(
						"Error encountered while closing channelContext %s", new Object[]{channelContext}), var24);
				response = this.buildResponse(var24, Status.INTERNAL_SERVER_ERROR);
			}

		}

		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					this.formatter.formatMessage(
							"Exiting fetchTransactions of UPDetails REST service, upDetailsResponse: %s",
							new Object[]{upDetailsResponse}));
		}

		return response;
	}
	
	
	
	@PATCH
	@Produces({"application/json"})
	@Path("/{partyId}/{udf2}")
	@Operation(summary = "Updates UDF2 of the customer", description = "Updates UDF2 of the customer", operationId = "com.ecobank.digx.cz.appx.UPDetails.service")

	public Response postUPDetails(@PathParam("partyId") Party party, String Udf2) throws java.lang.Exception 
		 {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, this.formatter.formatMessage(
					"Entered into fetchTransactions in REST %s: partyId=%s",
					new Object[]{THIS_COMPONENT_NAME, party }));
		}

		ChannelContext channelContext = null;
		Response response = null;
		UPDetailsResponseDTO upDetailsResponse = new UPDetailsResponseDTO();
		ChannelInteraction channelInteraction = ChannelInteraction.getInstance();

		try {
			channelContext = super.getChannelContext();
			channelInteraction.begin(channelContext);
		
			com.ecobank.digx.cz.app.UPDetails.service.UPDetails upDetailsService = new com.ecobank.digx.cz.app.UPDetails.service.UPDetails();
			upDetailsResponse  = upDetailsService.postUPDetails(channelContext.getSessionContext(),party, Udf2);
			response = this.buildResponse(upDetailsResponse, Response.Status.OK);
		} catch (Exception var25) {
			logger.log(Level.SEVERE,
					this.formatter.formatMessage(
							"Exception encountered while invoking the service %s for upDetailsRequestDTO=%s",
							new Object[]{UPDetails.class.getName(), party}),
					var25);
			response = this.buildResponse(var25, Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception var24) {
				logger.log(Level.SEVERE, this.formatter.formatMessage(
						"Error encountered while closing channelContext %s", new Object[]{channelContext}), var24);
				response = this.buildResponse(var24, Status.INTERNAL_SERVER_ERROR);
			}

		}

		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					this.formatter.formatMessage(
							"Exiting fetchTransactions of UPDetails REST service, upDetailsResponse: %s",
							new Object[]{upDetailsResponse}));
		}

		return response;
	}
	




}
