package com.ecobank.digx.cz.appx.UPDetails.service;

import javax.ws.rs.core.Response;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.*;
import com.ofss.digx.app.party.dto.PartyDTO;
import com.ofss.digx.datatype.complex.Party;

import io.swagger.v3.oas.annotations.tags.Tag;

import com.ofss.digx.datatype.complex.Account;


public interface IUPDetails {
	
	Response fetchUPDetails(Party var1) throws Exception;

	
	//Request modifyUPDetails(Party var1, String var2) throws Exception;

}
