package com.ecobank.digx.cz.app.UPDetails.dto;

import com.ofss.extsystem.dto.ResponseDTO;

public class UPDetailsExtResponseDTO extends ResponseDTO {
	
	private static final long serialVersionUID = 1L;

	private String Udf2;

	public String getUdf2() {
		return Udf2;
	}

	public void setUdf2(String udf2) {
		Udf2 = udf2;
	}
	
	


}
