package com.ecobank.digx.cz.app.UPDetails.service.ext;

import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsResponseDTO;
import com.ofss.digx.datatype.complex.Party;
import com.ofss.fc.app.context.SessionContext;

public class VoidUPDetailsExt implements IUPDetailsExt {
	public void preFetchUPDetails(SessionContext sessionContext,
			Party party) throws Exception {
	}

	public void postFetchUPDetails(SessionContext sessionContext,
			Party party,
			UPDetailsResponseDTO upDetailsResponseDTO) throws Exception {
	}
	
	public void prePostUPDetails(SessionContext sessionContext, Party party, String Udf2) throws Exception{
		
	}

	public void postPostUPDetails(SessionContext sessionContext, Party party, String Udf2,
			UPDetailsResponseDTO upDetailsResponseDTO) throws Exception{
		
	}


}
