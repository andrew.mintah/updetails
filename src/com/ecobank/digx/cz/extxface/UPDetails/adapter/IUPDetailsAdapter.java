package com.ecobank.digx.cz.extxface.UPDetails.adapter;



import com.ecobank.digx.cz.app.UPDetails.dto.*;

public interface IUPDetailsAdapter {

	
	
	
	UPDetailsResponseDTO readParty(String var1) throws Exception;

	
	UPDetailsResponseDTO updateCustomerDetail (CustomerUpdateRequestDTO custUpdateInfo) throws Exception;
}
