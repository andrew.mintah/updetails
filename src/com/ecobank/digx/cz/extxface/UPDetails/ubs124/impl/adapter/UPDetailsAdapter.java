package com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.adapter;

import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import com.ecobank.digx.cz.app.UPDetails.dto.CustomerUpdateRequestDTO;
import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsExtResponseDTO;
import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsResponseDTO;
import com.ecobank.digx.cz.extxface.UPDetails.adapter.IUPDetailsAdapter;
import com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.assembler.UPDetailsAssembler;
import com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.responsehandler.UPDetailsAdapterResponseHandler;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.extxface.adapter.AbstractAdapter;
import com.ofss.extsystem.business.extsystems.HostAdapterManager;
import com.ofss.extsystem.dto.HostRequestDTO;
import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.infra.ws.JAXWSFactory;

import com.ofss.fcubs124.gw.ws.types.fcubscustomerservice.FCUBSCustomerService;
import com.ofss.fcubs124.gw.ws.types.fcubscustomerservice.FCUBSCustomerServiceSEI;
import com.ofss.fcubs124.service.fcubscustomerservice.ERRORType;
import com.ofss.fcubs124.service.fcubscustomerservice.FCUBSHEADERType;
import com.ofss.fcubs124.service.fcubscustomerservice.MODIFYCUSTOMERFSFSREQ;
import com.ofss.fcubs124.service.fcubscustomerservice.MODIFYCUSTOMERFSFSRES;
import com.ofss.fcubs124.service.fcubscustomerservice.UBSCOMPType;
import com.thoughtworks.xstream.XStream;

public class UPDetailsAdapter extends AbstractAdapter implements IUPDetailsAdapter {
	
	private static final String THIS_COMPONENT_NAME = UPDetailsAdapter.class.getName();
	private final transient MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	private transient Logger logger  = this.formatter.getLogger(THIS_COMPONENT_NAME);
	private UPDetailsAdapterResponseHandler responseHandler = new UPDetailsAdapterResponseHandler();



	@Override
	public UPDetailsResponseDTO readParty(String var1) throws Exception {	
	
		UPDetailsResponseDTO responseDTO = null;
		super.checkRequest("com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.adapter",
				new Object[]{var1});
		AdapterInteraction.begin();
	    HostResponseDTO hostResponse = null;
	    HostRequestDTO hostRequest = null;
	    try {
	      System.out.println("fetch customer detail UDF - " + var1);
	      UPDetailsAssembler assembler = new UPDetailsAssembler();
	      hostRequest = assembler.fromAdaptertoHostRequestFetchCustomerDetail(var1, "ENG");
	      hostResponse = HostAdapterManager.processRequest(hostRequest);
	      UPDetailsExtResponseDTO custResponse = (UPDetailsExtResponseDTO)hostResponse.response;
	      System.out.println("fetch customer detail UDF Result " + custResponse.getUdf2());
	      responseDTO = new UPDetailsResponseDTO();
	      responseDTO.setUdf2(custResponse.getUdf2());
	    } catch (Exception e) {
	      if (logger.isLoggable(Level.SEVERE))
	        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed fetch UDF", new Object[0]), e); 
	    } finally {
	      AdapterInteraction.close();
	    } 
	    checkResponse(responseDTO);
	    return responseDTO;
		
		
	

	}
	
	public UPDetailsResponseDTO updateCustomerDetail (CustomerUpdateRequestDTO custUpdateInfo) throws Exception {
	
		UPDetailsResponseDTO responseDTO = new UPDetailsResponseDTO();
		super.checkRequest("com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.adapter",
				new Object[]{custUpdateInfo});
		AdapterInteraction.begin();
	    //HostResponseDTO hostResponse = null;
	    MODIFYCUSTOMERFSFSREQ hostRequest = null;
	    MODIFYCUSTOMERFSFSRES response = null;
	    XStream xs = new XStream();
	    try {
	      System.out.println("post customer detail UDF - " + custUpdateInfo.getCustomerId());
	      UPDetailsAssembler assembler = new UPDetailsAssembler();
	      hostRequest = assembler.fromModifyPartyDetailsDTOTOUBSObjectRequest(custUpdateInfo);
	      
	      System.out.println("post request: " + xs.toXML(hostRequest));
	      
			String url = "http://10.4.139.86:7004/FCUBSCustomerService/FCUBSCustomerService?wsdl";
			URL wsdlURL = new URL(url);
			QName qName = new QName("http://FCUBSCustomerService.types.ws.gw.fcubs.ofss.com", "FCUBSCustomerService");
			FCUBSCustomerService service = new FCUBSCustomerService(wsdlURL,qName);
			FCUBSCustomerServiceSEI port = service.getFCUBSCustomerServiceSEI();
			
			BindingProvider bindp = ((BindingProvider) port);
			Map<String, Object> requestContext = bindp.getRequestContext();
			
	        //FCUBSCustomerServiceSEI customerService = (FCUBSCustomerServiceSEI)  JAXWSFactory.createServiceStub("FCUBSCustomerServiceSEI", "modifyCustomer");
	     
	      response = port.modifyCustomerFS(hostRequest);
	      
	      System.out.println("post response: " + xs.toXML(response)); 
	      
	      FCUBSHEADERType rspHead = response.getFCUBSHEADER();
			String rspCode = rspHead.getMSGSTAT().value().toString();

			System.out.println("Response: " + rspHead.getMSGSTAT() ); //+ "  " + xs.toXML(resp));
			System.out.println("Respone2 " + response.getFCUBSBODY().getCustomerFull().getCUSTNO());

			if (rspCode.equals("SUCCESS")) {
				
				responseDTO.setResponseCode("000");
				responseDTO.setResponseMessage("SUCCESS");

				
			} else {
				
				String errMsg = "";
				ERRORType[] errors = response.getFCUBSBODY().getFCUBSERRORRESP();
				if(errors != null && errors.length > 0)
				{
					
					for(ERRORType err : errors)
					{
						//for(ERRORDETAILSTYPE errType : err.getERROR())
						errMsg += err.getERROR(0).getECODE() + " " + err.getERROR(0).getEDESC();
					}
				}
				
				responseDTO.setResponseCode("E04");
				responseDTO.setResponseMessage(errMsg);
				
		
			}
	      
	      
	     // hostResponse = HostAdapterManager.processRequest(hostRequest);
	      /*UPDetailsExtResponseDTO custResponse = (UPDetailsExtResponseDTO)hostResponse.response;
	      System.out.println("post customer detail UDF Result " + custResponse.getUdf2());
	      responseDTO = new UPDetailsResponseDTO();
	      responseDTO.setUdf2(custResponse.getUdf2());*/
	    } catch (Exception e) {
	      if (logger.isLoggable(Level.SEVERE))
	        logger.log(Level.SEVERE, this.formatter.formatMessage("Remote Adapter invocation failed post UDF", new Object[0]), e); 
	    } finally {
	      AdapterInteraction.close();
	    } 
	    checkResponse(responseDTO);
	    return responseDTO;
			}


}
