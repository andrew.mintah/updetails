package com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.adapter;


import com.ecobank.digx.cz.extxface.UPDetails.adapter.IUPDetailsAdapter;
import com.ofss.digx.app.account.adapter.impl.AccountAdapterFactory;
import com.ofss.digx.app.adapter.AdapterFactory;
import com.ofss.digx.datatype.NameValuePair;

public class UPDetailsAdapterFactory extends AdapterFactory {
	private static UPDetailsAdapterFactory adapterFactory = null;

	public static UPDetailsAdapterFactory getInstance() {
		     if (adapterFactory == null) {
		       synchronized (AccountAdapterFactory.class) {
		         adapterFactory = new UPDetailsAdapterFactory();
		       } 
		     }
		     return adapterFactory;
		  }
	public Object getAdapter(String adapterClass) {
		IUPDetailsAdapter adapter = null;
		System.out.println("Inside Adapter Factory Manu");
	if (adapterClass.equals("UPDetailsAdapter")) {
			adapter = new UPDetailsAdapter();
			
		}

		return adapter;
	}

	public Object getAdapter(String adapter, NameValuePair[] nameValues) {
		return null;
	}


}
