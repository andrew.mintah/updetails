package com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.responsehandler;

import java.awt.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ecobank.digx.cz.extxface.UPDetails.ubs124.impl.adapter.UPDetailsAdapter;
import com.ofss.digx.app.messages.Message;
import com.ofss.digx.extxface.response.AbstractResponseHandler;

import com.ofss.extsystem.dto.HostResponseDTO;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fcubs124.service.fcubscustomerservice.ERRORDETAILSType;
import com.ofss.fcubs124.service.fcubscustomerservice.ERRORType;
import com.ofss.fcubs124.service.fcubscustomerservice.MODIFYCUSTOMERFSFSRES;
import com.ofss.fcubs124.service.fcubscustomerservice.MsgStatType;
import com.ofss.fcubs124.service.fcubscustomerservice.QUERYCUSTOMERIOFSREQ;
import com.ofss.fcubs124.service.fcubscustomerservice.QUERYCUSTOMERIOFSRES;



public class UPDetailsAdapterResponseHandler extends AbstractResponseHandler{
	private static final String THIS_COMPONENT_NAME = UPDetailsAdapterResponseHandler.class.getName();
	private static final MultiEntityLogger FORMATTER = MultiEntityLogger.getUniqueInstance();
	private static transient Logger logger;




	public void checkResponseUpdateProfileDetails(MODIFYCUSTOMERFSFSRES modifyCUSTOMERFSFSRES) throws Exception {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					FORMATTER.formatMessage(
							"Entered in method checkResponseUpdateProfileDetails of %s, modifyCUSTOMERFSFSRES = %s",
							new Object[]{THIS_COMPONENT_NAME, modifyCUSTOMERFSFSRES}));
		}

		if (modifyCUSTOMERFSFSRES == null) {
			this.analyseResponse("DIGX_PROD_DEF_0000", this.getClass());
		} else if (!modifyCUSTOMERFSFSRES.getFCUBSHEADER().getMSGSTAT().equals(MsgStatType.SUCCESS)
				&& modifyCUSTOMERFSFSRES.getFCUBSBODY().getFCUBSERRORRESP() != null
				&& modifyCUSTOMERFSFSRES.getFCUBSBODY().getFCUBSERRORRESP().length > 0) {
			if (!modifyCUSTOMERFSFSRES.getFCUBSBODY().getFCUBSERRORRESP()[0].getERROR(0).getECODE()
					.equals("ST-AMND-002")
					&& !modifyCUSTOMERFSFSRES.getFCUBSBODY().getFCUBSERRORRESP()[0].getERROR(0).getECODE()
							.equals("ST-CIF-310")) {
				this.analyseResponse("DIGX_PROD_DEF_0000", this.getClass());
			} else {
				this.analyseResponse("DIGX_PROD_DEF_0000", this.getClass());
			}
		}

		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					FORMATTER.formatMessage(
							"Exiting in method checkResponseUpdateProfileDetails of %s, modifyCUSTOMERFSFSRES is = %s",
							new Object[]{THIS_COMPONENT_NAME, modifyCUSTOMERFSFSRES}));
		}

	}

	static {
		logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	}
}




