package com.ecobank.digx.cz.app.UPDetails.service.ext;

import java.util.List;

import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsResponseDTO;
import com.ecobank.digx.cz.app.UPDetails.service.UPDetails;
import com.ofss.digx.app.ext.ServiceExtensionFactory;
import com.ofss.digx.datatype.complex.Party;
import com.ofss.fc.app.context.SessionContext;

public class UPDetailsExtExecutor implements IUPDetailsExtExecutor {
    private List<IUPDetailsExt> serviceExtensions;
    
    public UPDetailsExtExecutor()
    {
    	this.serviceExtensions = ServiceExtensionFactory.getServiceExtensions(UPDetails.class.getName());
    	
    }
    
    public void preFetchUPDetails(SessionContext sessionContext,
			String party) throws Exception {
		for (int i = 0; i < this.serviceExtensions.size(); ++i) {
			((IUPDetailsExt) this.serviceExtensions.get(i)).preFetchUPDetails(sessionContext,
					party);
		}

	}

	public void postFetchUPDetails(SessionContext sessionContext,
			String party,
			UPDetailsResponseDTO upDetailsResponseDTO) throws Exception {
		for (int i = 0; i < this.serviceExtensions.size(); ++i) {
			((IUPDetailsExt) this.serviceExtensions.get(i)).postFetchUPDetails(sessionContext,
					party, upDetailsResponseDTO);
		}

	}
	
    public void prePostUPDetails(SessionContext sessionContext,
			String party, String Udf2) throws Exception {
		for (int i = 0; i < this.serviceExtensions.size(); ++i) {
			((IUPDetailsExt) this.serviceExtensions.get(i)).prePostUPDetails(sessionContext,
					party, Udf2);
		}

	}

	public void postPostUPDetails(SessionContext sessionContext,
			String party,String Udf2,
			UPDetailsResponseDTO upDetailsResponseDTO) throws Exception {
		for (int i = 0; i < this.serviceExtensions.size(); ++i) {
			((IUPDetailsExt) this.serviceExtensions.get(i)).postPostUPDetails(sessionContext,
					party, Udf2, upDetailsResponseDTO);
		}

	}


	
	
	



}
