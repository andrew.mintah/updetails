package com.ecobank.digx.cz.app.UPDetails.service.ext;


import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsResponseDTO;
import com.ofss.digx.datatype.complex.Party;
import com.ofss.fc.app.context.SessionContext;

public interface IUPDetailsExt {
	void preFetchUPDetails(SessionContext var1, String var2) throws Exception;

	void postFetchUPDetails(SessionContext var1, String var2,
			UPDetailsResponseDTO var3) throws Exception;
	
	
	void prePostUPDetails(SessionContext var1, String var2, String var3) throws Exception;

	void postPostUPDetails(SessionContext var1, String var2, String var3,
			UPDetailsResponseDTO var4) throws Exception;



}
