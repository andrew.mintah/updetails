package com.ecobank.digx.cz.app.UPDetails.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ecobank.digx.cz.app.UPDetails.dto.CustomerUpdateRequestDTO;
import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsResponseDTO;
import com.ecobank.digx.cz.app.UPDetails.service.ext.IUPDetailsExtExecutor;
import com.ecobank.digx.cz.extxface.UPDetails.adapter.IUPDetailsAdapter;
import com.ofss.digx.app.AbstractApplication;
import com.ofss.digx.app.Interaction;
import com.ofss.digx.app.adapter.AdapterFactoryConfigurator;
import com.ofss.digx.app.adapter.IAdapterFactory;
import com.ofss.digx.app.ext.ServiceExtensionFactory;
import com.ofss.digx.app.user.dto.UserProfileUpdateRequestDTO;
import com.ofss.digx.app.user.service.UserPartyResponse;
import com.ofss.digx.datatype.complex.Party;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.digx.infra.thread.ThreadAttribute;
import com.ofss.fc.app.context.SessionContext;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.service.response.TransactionStatus;
import com.thoughtworks.xstream.XStream;

public class UPDetails extends AbstractApplication implements IUPDetails {

	private static final String THIS_COMPONENT_NAME = UPDetails.class.getName();
	private IUPDetailsExtExecutor extensionExecutor = (IUPDetailsExtExecutor) ServiceExtensionFactory
			.getServiceExtensionExecutor(THIS_COMPONENT_NAME);
	private MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();
	private transient Logger LOGGER = this.formatter.getLogger(THIS_COMPONENT_NAME);
	private Object executor;

	public UPDetailsResponseDTO fetchUPDetails(SessionContext sessionContext, String party) throws Exception {

		if (LOGGER.isLoggable(Level.FINE)) {
			LOGGER.log(Level.FINE,
					this.formatter.formatMessage("Entering fetchUPDetails of %s, upDetailsRequestDTO:%s ",
							new Object[] { THIS_COMPONENT_NAME, party }));
		}

		// super.checkAccessPolicy("com.ecobank.digx.cz.app.UPDetails.service.UPDetails.fetchUPDetails",
		// new Object[]{sessionContext, party});
		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = this.fetchTransactionStatus();
		UPDetailsResponseDTO upDetailsResponseDTO = new UPDetailsResponseDTO();

		try {
			//this.extensionExecutor.preFetchUPDetails(sessionContext, party);
			this.extensionExecutor.preFetchUPDetails(sessionContext, party);

			// IAdapterFactory upDetailsAdapterFactory = (IAdapterFactory)
			// AdapterFactoryConfigurator.getInstance()
			// .getAdapterFactory("UPDETAILS_ADAPTER_FACTORY");

			IAdapterFactory upDetailsAdapterFactory = AdapterFactoryConfigurator.getInstance()
					.getAdapterFactory("UPDETAILS_ADAPTER_FACTORY");

			IUPDetailsAdapter upDetailsAdapter = (IUPDetailsAdapter) upDetailsAdapterFactory
					.getAdapter("UPDetailsAdapter");

			upDetailsResponseDTO = upDetailsAdapter.readParty(party);
			upDetailsResponseDTO.setStatus(this.buildStatus(transactionStatus));

		//	this.extensionExecutor.postFetchUPDetails(sessionContext, party, upDetailsResponseDTO);
			this.extensionExecutor.postFetchUPDetails(sessionContext, party, upDetailsResponseDTO);
			

		} catch (Exception var31) {
			LOGGER.log(Level.SEVERE, this.formatter.formatMessage(
					"Exception has occured while getting response object inside the fetchUPDetails method of %s .Exception details are %s",
					new Object[] { UPDetails.class, var31 }));
			this.fillTransactionStatus(transactionStatus, var31);
		} catch (RuntimeException var32) {
			this.fillTransactionStatus(transactionStatus, var32);
			LOGGER.log(Level.SEVERE,
					this.formatter.formatMessage("RunTimeException from fetchUPDetails for upDetailsRequestdto"),
					var32);
		} catch (java.lang.Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			Interaction.close();
		}

		super.checkResponsePolicy(sessionContext, upDetailsResponseDTO);
		super.encodeOutput(upDetailsResponseDTO);

		return upDetailsResponseDTO;
	}

	public UPDetailsResponseDTO updateCustomerDetail(SessionContext sessionContext, CustomerUpdateRequestDTO custUpdateInfo)
			throws Exception {

		if (LOGGER.isLoggable(Level.FINE)) {
			LOGGER.log(Level.FINE, this.formatter.formatMessage("Entering PostUPDetails of %s, upDetailsRequestDTO:%s ",
					new Object[] { THIS_COMPONENT_NAME, custUpdateInfo }));
		}

		// super.checkAccessPolicy("com.ecobank.digx.cz.app.UPDetails.service.UPDetails.fetchUPDetails",
		// new Object[]{sessionContext, party});
		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = this.fetchTransactionStatus();
		UPDetailsResponseDTO upDetailsResponseDTO = new UPDetailsResponseDTO();

		try {
			// this.extensionExecutor.prePostUPDetails(sessionContext, party, Udf2);

			// IAdapterFactory upDetailsAdapterFactory = (IAdapterFactory)
			// AdapterFactoryConfigurator.getInstance()
			// .getAdapterFactory("UPDETAILS_ADAPTER_FACTORY");
			XStream xs = new XStream();
			System.out.println(" Customer Update " + xs.toXML(custUpdateInfo));

			IAdapterFactory upDetailsAdapterFactory = AdapterFactoryConfigurator.getInstance()
					.getAdapterFactory("UPDETAILS_ADAPTER_FACTORY");

			IUPDetailsAdapter upDetailsAdapter = (IUPDetailsAdapter) upDetailsAdapterFactory
					.getAdapter("UPDetailsAdapter");

			Party partyId = new Party();
			partyId.setValue(custUpdateInfo.getCustomerId());

			com.ofss.digx.app.user.service.User user = new com.ofss.digx.app.user.service.User();
			UserPartyResponse userPartyResponse = user.fetchPartyInformation(sessionContext, partyId);
			// String userName = (String) ThreadAttribute.get("SUBJECT_NAME");
			// com.ofss.digx.app.user.service.User user = new
			// com.ofss.digx.app.user.service.User();

			// new Party(sessionContext.getTransactingPartyCode()

			// This is code to update customer email in User Profile in OBDX
			UserProfileUpdateRequestDTO userProfileUpdateRequestDTO = new UserProfileUpdateRequestDTO();
			userProfileUpdateRequestDTO.setAttr_name("EMAIL");
			userProfileUpdateRequestDTO.setAttr_value(custUpdateInfo.getEmail());
			user.updateProfile(sessionContext, userProfileUpdateRequestDTO);

			// userProfileUpdateRequestDTO.getAttr_name().equalsIgnoreCase("EMAIL")
			// userProfileUpdateRequestDTO.getAttr_name().equalsIgnoreCase("PHONENO")

			// System.out.println("Cust Name 222 :::" + xs.toXML(userPartyResponse));
			// String email = "", mobile = "";
			// String country = "", city = "";

			// String custName =
			// userPartyResponse.getParty().getPersonalDetails().getFullName();
			// Date dob = userPartyResponse.getParty().getPersonalDetails().getBirthDate();

			// CustomerUpdateRequestDTO custUpdateInfo = new CustomerUpdateRequestDTO();

			// This is code to update customer information on Flexcube
			System.out.println("Update customer on Flexcube : " + custUpdateInfo.getAddressLine1());
			upDetailsResponseDTO = upDetailsAdapter.updateCustomerDetail(custUpdateInfo); // (party.getValue(),Udf2);
			upDetailsResponseDTO.setStatus(this.buildStatus(transactionStatus));

			// this.extensionExecutor.postPostUPDetails(sessionContext, party, Udf2,
			// upDetailsResponseDTO);

		} catch (Exception var31) {
			LOGGER.log(Level.SEVERE, this.formatter.formatMessage(
					"Exception has occured while getting response object inside the PostUPDetails method of %s .Exception details are %s",
					new Object[] { UPDetails.class, var31 }));
			this.fillTransactionStatus(transactionStatus, var31);
		} catch (RuntimeException var32) {
			this.fillTransactionStatus(transactionStatus, var32);
			LOGGER.log(Level.SEVERE,
					this.formatter.formatMessage("RunTimeException from PostUPDetails for upDetailsRequestdto"), var32);
		} catch (java.lang.Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			Interaction.close();
		}

		super.checkResponsePolicy(sessionContext, upDetailsResponseDTO);
		super.encodeOutput(upDetailsResponseDTO);

		return upDetailsResponseDTO;
		// return this.fetchTransactionStatus();

	}

	public UPDetailsResponseDTO postUPDetails(SessionContext sessionContext, String partyId, String udf)
			throws Exception {

		if (LOGGER.isLoggable(Level.FINE)) {
			LOGGER.log(Level.FINE, this.formatter.formatMessage("Entering PostUPDetails of %s, upDetailsRequestDTO:%s ",
					new Object[] { THIS_COMPONENT_NAME, partyId }));
		}

		// super.checkAccessPolicy("com.ecobank.digx.cz.app.UPDetails.service.UPDetails.fetchUPDetails",
		// new Object[]{sessionContext, party});
		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = this.fetchTransactionStatus();
		UPDetailsResponseDTO upDetailsResponseDTO = new UPDetailsResponseDTO();

		try {
			// this.extensionExecutor.prePostUPDetails(sessionContext, party, Udf2);

			// IAdapterFactory upDetailsAdapterFactory = (IAdapterFactory)
			// AdapterFactoryConfigurator.getInstance()
			// .getAdapterFactory("UPDETAILS_ADAPTER_FACTORY");
			XStream xs = new XStream();
			//System.out.println(" Customer Update " + xs.toXML(custUpdateInfo));

			IAdapterFactory upDetailsAdapterFactory = AdapterFactoryConfigurator.getInstance()
					.getAdapterFactory("UPDETAILS_ADAPTER_FACTORY");

			IUPDetailsAdapter upDetailsAdapter = (IUPDetailsAdapter) upDetailsAdapterFactory
					.getAdapter("UPDetailsAdapter");

			Party party = new Party();
			party.setValue(partyId);
            
			CustomerUpdateRequestDTO custUpdateInfo = new CustomerUpdateRequestDTO();
			custUpdateInfo.setCustomerId(partyId);
			custUpdateInfo.setUdf(udf);

			// This is code to update customer information on Flexcube
			upDetailsResponseDTO = upDetailsAdapter.updateCustomerDetail(custUpdateInfo); // (party.getValue(),Udf2);
			upDetailsResponseDTO.setStatus(this.buildStatus(transactionStatus));

			// this.extensionExecutor.postPostUPDetails(sessionContext, party, Udf2,
			// upDetailsResponseDTO);

		} catch (Exception var31) {
			LOGGER.log(Level.SEVERE, this.formatter.formatMessage(
					"Exception has occured while getting response object inside the PostUPDetails method of %s .Exception details are %s",
					new Object[] { UPDetails.class, var31 }));
			this.fillTransactionStatus(transactionStatus, var31);
		} catch (RuntimeException var32) {
			this.fillTransactionStatus(transactionStatus, var32);
			LOGGER.log(Level.SEVERE,
					this.formatter.formatMessage("RunTimeException from PostUPDetails for upDetailsRequestdto"), var32);
		} catch (java.lang.Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			Interaction.close();
		}

		super.checkResponsePolicy(sessionContext, upDetailsResponseDTO);
		super.encodeOutput(upDetailsResponseDTO);

		return upDetailsResponseDTO;
		// return this.fetchTransactionStatus();

	}

}
