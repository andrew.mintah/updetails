package com.ecobank.digx.cz.app.UPDetails.service;

import com.ecobank.digx.cz.app.UPDetails.dto.CustomerUpdateRequestDTO;
import com.ecobank.digx.cz.app.UPDetails.dto.UPDetailsResponseDTO;
import com.ofss.digx.datatype.complex.Party;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.app.context.SessionContext;

public interface IUPDetails {
	
	public UPDetailsResponseDTO fetchUPDetails(SessionContext sessionContext, String party) throws Exception;
	
	public UPDetailsResponseDTO updateCustomerDetail(SessionContext sessionContext, CustomerUpdateRequestDTO custUpdateInfo)
			throws Exception;
	
	public UPDetailsResponseDTO postUPDetails(SessionContext sessionContext, String party, String udf)
			throws Exception;

}
