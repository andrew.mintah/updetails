package com.ecobank.digx.cz.app.UPDetails.dto;

import com.ofss.digx.service.response.BaseResponseObject;

public class UPDetailsResponseDTO extends BaseResponseObject {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String udf2;
	
	private String responseCode;
	private String responseMessage;
	
	
	public String getUdf2() {
		return udf2;
	}
	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	
	

}
