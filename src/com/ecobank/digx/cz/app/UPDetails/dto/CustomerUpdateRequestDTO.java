package com.ecobank.digx.cz.app.UPDetails.dto;


import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class CustomerUpdateRequestDTO extends DataTransferObject {

	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = CustomerUpdateRequestDTO.class.getName();
	private transient MultiEntityLogger formatter;

	public CustomerUpdateRequestDTO() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}
	
	private String alias;
	private String udf;
	private String email;
	private String mobileNo;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String addressLine4;
	
	private String photo;
	
	private String customerId;

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getUdf() {
		return udf;
	}

	public void setUdf(String udf) {
		this.udf = udf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getAddressLine4() {
		return addressLine4;
	}

	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	

}
